#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <pthread.h>

//////////////////////////////////////

static unsigned long compteur_gc(char *bloc, unsigned long taille) {
    unsigned long i, cptr = 0;

    for (i = 0; i < taille; i++)
        if (bloc[i] == 'G' || bloc[i] == 'C')
            cptr++;

    return cptr;
}

//////////////////////////////////////

typedef struct {
    char* bloc;
    unsigned int taille;
    unsigned long resultat;
} compteur_param_t;

static void* compteur_gc_wrapper(void* param) {
    compteur_param_t* cparam = (compteur_param_t*)param;
    cparam->resultat = compteur_gc(cparam->bloc, cparam->taille);
    return NULL;
}

//////////////////////////////////////

int main(int argc, char *argv[]) {
    struct stat st;
    int fd;
    char *tampon;
    int lus;
    unsigned long cptr = 0;
    off_t taille = 0;
    struct timespec debut, fin;

    assert("Usage: ./compteur-gc-par <file> <nb_threads>" && argc > 1);

    /* Quelle taille ? */
    assert(stat(argv[1], &st) != -1);
    tampon = malloc(st.st_size);
    assert(tampon != NULL);

    /* Chargement en mémoire */
    fd = open(argv[1], O_RDONLY);
    assert(fd != -1);
    while ((lus = read(fd, tampon + taille, st.st_size - taille)) > 0)
        taille += lus;
    assert(lus != -1);
    assert(taille == st.st_size);
    close(fd);

    // on setup les threads + on les lances
    const int nb_threads = atoi(argv[2]);
    assert("Nombre de threads <= 0" && nb_threads > 0);
    const size_t thread_read_size = taille / nb_threads;
    pthread_t threads[nb_threads];
    compteur_param_t tparams[nb_threads];
    for(int t = 0; t < nb_threads; ++t) {
        tparams[t].bloc     = &tampon[t * thread_read_size];
        tparams[t].taille   = thread_read_size;
        tparams[t].resultat = 0u;
    }

    /* Calcul proprement dit */
    assert(clock_gettime(CLOCK_MONOTONIC, &debut) != -1);
    for(int t = 0; t < nb_threads; ++t) {
        assert(pthread_create(&threads[t], NULL, compteur_gc_wrapper, (void*)&tparams[t]) == 0);
    }
    // on recupère leur resultat
    for(int t = 0; t < nb_threads; ++t) {
        pthread_join(threads[t], NULL);
        cptr += tparams[t].resultat;
    }
    assert(clock_gettime(CLOCK_MONOTONIC, &fin) != -1);

    /* Affichage des résultats */
    printf("Nombres de GC:   %ld\n", cptr);
    printf("Taux de GC:      %lf\n", ((double) cptr) / ((double) taille));

    fin.tv_sec  -= debut.tv_sec;
    fin.tv_nsec -= debut.tv_nsec;
    if (fin.tv_nsec < 0) {
        fin.tv_sec--;
        fin.tv_nsec += 1000000000;
    }
    printf("Durée de calcul: %ld.%09ld\n", fin.tv_sec, fin.tv_nsec);
    printf("(Attention: très peu de chiffres après la virgule sont réellement significatifs !)\n");

    free(tampon);
    
    return 0;
}
