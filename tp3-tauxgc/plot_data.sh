ouput=res.dat
cptr_command=./compteur-gc-par
gn_command=./aleazard
gn_file=genome_tmp.txt

for (( gn=0; gn<9; gn++ ))
do
    nb_gn=$(( 10**$gn ))
    echo "Writing $nb_gn genomes"
    $gn_command $nb_gn > $gn_file
    for (( th=0; th<9; th++ ))
    do
        nb_th=$(( 2**th ))
        echo "Testing $nb_gn genomes with $nb_th threads"
        res=$($cptr_command $gn_file $nb_th | grep "Durée" | awk '{ print $4; }')
        echo "$nb_gn $nb_th $res" >> $ouput
    done
done

# gnuplot plot_dat.plt