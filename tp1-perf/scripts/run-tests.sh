#!/bin/bash
# Script shell qui mesure le temps d'exécution de
# mcat-XXX pour un fichier donné

FILE=data/large.tgz
FILE_SIZE=$(ls -lh $FILE | awk '{ print $5 }')
FILE_SIZE_OCTETS=$(stat -c%s $FILE)
printf "Running for $1 with $FILE (size: $FILE_SIZE)\n"
export MCAT_BUFSIZ 
for((i = 0; i < 10; i++)); do
    val=$((2**"$i"))
    time_spend=$($(MCAT_BUFSIZ="$val" /usr/bin/time -f '%e' "$1" "$FILE" 1> /dev/null) | awk '{ print $1 }')
    byte_second=$(( FILE_SIZE_OCTETS / time_spend ))
    printf '%d %f\n' "$time_spend" "$byte_second"
done
