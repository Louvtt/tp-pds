#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char** argv)
{
    int c;
    for(int i = 1; i < argc; ++i)
    {
        FILE* f = fopen(argv[i], "r");
        if(f == NULL) { 
            fprintf(stderr, "Failed to open a file\n"); 
        } else {
            while((c = fgetc(f)) != EOF)
            {
                fputc(c, stdout);
            }
            fclose(f);
            printf("\n");
        }
    }
    exit(EXIT_SUCCESS);
}