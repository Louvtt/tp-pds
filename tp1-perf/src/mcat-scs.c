#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#define MCAT_BUFSIZ 512
static char buffer[MCAT_BUFSIZ];

int main(int argc, char** argv)
{
    for(int i = 1; i < argc; ++i)
    {
        int fd = open(argv[i], O_RDONLY);
        if(fd == -1) { 
            fprintf(stderr, "Failed to open a file\n"); 
        } else {
            int count = 0;
            while((count = read(fd, buffer, MCAT_BUFSIZ)) > 0)
            {
                write(1, buffer, count);
            }
            printf("\n");
            close(fd);
        }
    }
    exit(EXIT_SUCCESS);
}