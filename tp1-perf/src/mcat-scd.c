#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char** argv)
{
    int MCAT_BUFSIZ = 256;
    char* MCAT_BUFSIZ_str = getenv("MCAT_BUFSIZ");
    if(MCAT_BUFSIZ_str) {
        MCAT_BUFSIZ = atoi(MCAT_BUFSIZ_str);
        fprintf(stderr, "Found MCAT_BUFSIZ=%s\n", MCAT_BUFSIZ_str);
    } else {
        fprintf(stderr, "MCAT_BUFSIZ missing\n");
    }

    char buffer[MCAT_BUFSIZ]; // = malloc(MCAT_BUFSIZ * sizeof(char));
    for(int i = 1; i < argc; ++i)
    {
        int fd = open(argv[i], O_RDONLY);
        if(fd == -1) { 
            fprintf(stderr, "Failed to open a file\n"); 
        } else {
            int count = 0;
            while((count = read(fd, buffer, MCAT_BUFSIZ)) > 0)
            {
                write(1, buffer, count);
            }
            close(fd);
            printf("\n");
        }
    }
    exit(EXIT_SUCCESS);
}