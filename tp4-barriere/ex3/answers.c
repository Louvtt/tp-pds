#include "answers.h"
#include <assert.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>

void mbox_init(struct mailbox *mbox, int n)
{
    assert(mbox->magic != MAILBOX_MAGIC);
    mbox->magic   = MAILBOX_MAGIC;
    mbox->nb_cons = n;
    mbox->count   = 0;
    // init mutex             ↓ must be 1 for a mutex
    sem_init(&mbox->mutex, 1, 1);
    // init fence
    mbox->fences = (sem_t*)malloc(sizeof(sem_t) * n);
    for(int i = 0; i < mbox->nb_cons; ++i)
        sem_init(&mbox->fences[i], 1, 0);
    // init lock
    sem_init(&mbox->lock, 1, 0);
}

void mbox_put(struct mailbox *mbox, int d)
{
    sem_wait(&mbox->mutex);
    mbox->content = d; // critical section (write)
    mbox->count   = 0; // reset count
    sem_post(&mbox->mutex);
    // lock
    sem_post(&mbox->fences[0]);
    sem_wait(&mbox->lock);
}

int  mbox_get(struct mailbox *mbox, int index)
{
    // fence
    sem_wait(&mbox->fences[index]);
    if(index < mbox->nb_cons) {
        sem_post(&mbox->fences[index+1]);
    }

    // get content using mutex
    int content,count;
    sem_wait(&mbox->mutex);
    content = mbox->content; // critical section (read)
    count = ++mbox->count;
    sem_post(&mbox->mutex);

    // last consumer unlock mailbox
    if(count >= mbox->nb_cons) {
        sem_post(&mbox->lock);
    }
    
    return content;
}


void mbox_destroy(struct mailbox *mbox)
{
    assert(mbox->magic == MAILBOX_MAGIC);
    mbox->magic = 0; // deinit flag
    sem_destroy(&mbox->mutex);
    sem_destroy(&mbox->lock);
    for(int i = 0; i < mbox->nb_cons; ++i) {
        sem_destroy(&mbox->fences[i]);
    }
    free(mbox->fences);
}
