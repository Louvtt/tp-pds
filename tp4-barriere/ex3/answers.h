#pragma once

#include <semaphore.h>

#define MAILBOX_MAGIC 0xEEAF0110
struct mailbox {
    unsigned int magic; // init flag
    int count;   // current count of waiting consumers
    int nb_cons; // consumers count
    int content; // content of the mailbox
    sem_t mutex; // read/write operations
    sem_t* fences; // synchronisation
    sem_t lock;  // lock
}; 

void mbox_init(struct mailbox *mbox, int n);
void mbox_put(struct mailbox *mbox, int d);
int  mbox_get(struct mailbox *mbox, int index);
void mbox_destroy(struct mailbox *mbox);
