#include "answers_part1.h"
#include <assert.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>

void mbox_init(struct mailbox *mbox, int n)
{
    assert(mbox->magic != MAILBOX_MAGIC);
    mbox->magic   = MAILBOX_MAGIC;
    mbox->nb_cons = n;
    // init mutex             ↓ must be 1 for a mutex
    sem_init(&mbox->mutex, 0, 1);
    // init fence
    sem_init(&mbox->fence, 0, 0);
    // init lock
    sem_init(&mbox->lock, 0, 0);
}

void mbox_put(struct mailbox *mbox, int d)
{
    sem_wait(&mbox->mutex);
    mbox->content = d; // critical section (write)
    sem_post(&mbox->mutex);
    // lock
    sem_post(&mbox->fence);
    sem_wait(&mbox->lock);
}

int  mbox_get(struct mailbox *mbox, int index)
{
    // fence
    sem_wait(&mbox->fence);

    // get content using mutex
    int content;
    sem_wait(&mbox->mutex);
    content = mbox->content; // critical section (read)
    sem_post(&mbox->mutex);

    // unlock mailbox
    // if(count >= mbox->nb_cons)
    sem_post(&mbox->lock);
    
    return content;
}


void mbox_destroy(struct mailbox *mbox)
{
    assert(mbox->magic == MAILBOX_MAGIC);
    mbox->magic = 0; // deinit flag
    sem_destroy(&mbox->mutex);
    sem_destroy(&mbox->fence);
    sem_destroy(&mbox->lock);
}
