#include <semaphore.h>
#include "answers.h"

// Declarer les semaphores ici
static sem_t sem_a;
static sem_t sem_b;
static int initialised = 0;

void init()
{
    if(initialised) {
        sem_destroy(&sem_a);
        sem_destroy(&sem_b);
    }
    sem_init(&sem_a, 0, 0);
    sem_init(&sem_b, 0, 0);
    initialised = 1;
}

void synch(int i)
{
    switch(i) {
        case 0: // fb(0) 
            sem_wait(&sem_a); // wait for B(1)
            break;
        case 1: // fb(1)
            sem_wait(&sem_b); // wait for B(2)
            sem_post(&sem_a); // unlock B(0) after B(1)
            break;
        case 2: // fb(2)
            sem_post(&sem_b); // unlock B(1) after A(2) 
            break;
    }
}
