#include "answers.h"
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>

void barr_init(struct barrier *b, int n)
{
    if(b->magic == BARRIER_MAGIC) {
        sem_destroy(&b->mutex);
        sem_destroy(&b->barrier);
    }
    b->magic = BARRIER_MAGIC;
    b->n = n;
    b->count = 0;
    sem_init(&b->mutex, 0, 1);
    sem_init(&b->barrier, 0, 0);
}

void barr_synch(struct barrier *b)
{   
    // value read safe
    int count;
    sem_wait(&b->mutex);
    b->count++; // section critique
    count = b->count;
    sem_post(&b->mutex);

    if(count == b->n) {
        // unlock first thread
        sem_post(&b->barrier);
    } 
    // wait unlock
    sem_wait(&b->barrier);
    // unlock next
    sem_post(&b->barrier);

}

