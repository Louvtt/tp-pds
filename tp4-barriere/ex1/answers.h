#pragma once

#include <semaphore.h>

#define BARRIER_MAGIC 0xDEADBEEF
struct barrier {
    unsigned int magic;
    int n;
    int count;
    sem_t mutex;
    sem_t barrier;
};

void barr_init(struct barrier *b, int n);
void barr_synch(struct barrier *b);
