#include <linux/limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <dirent.h>
#include <string.h>
#include <limits.h>
#include <search.h>

/* Definition des variables globales */
int opt_follow_links = 0;
int opt_apparent_size = 0;

/** Utility function for exercice 4 */
static int opt_stat(const char *pathname, struct stat *buf)
{
    if(opt_follow_links) {
        return stat(pathname, buf);
    } else {
        return lstat(pathname, buf);
    }
}

/* Exercice 1 */
int du_single_file(const char *pathname)
{
    struct stat fstat;
    if(opt_stat(pathname, &fstat) == -1) {
        return -1;
    }
    return (opt_apparent_size ? 
          fstat.st_size 
        : fstat.st_blocks);    
}

/* Exercice 2 */
int is_dir(const char *pathname)
{
    struct stat fstat;
    if(opt_stat(pathname, &fstat) == -1) {
        return -1;
    }
    return S_ISDIR(fstat.st_mode);    
}


/* Exercice 3 */
int du_file(const char *pathname)
{
    size_t size = 0;
    if(is_dir(pathname)) {
        DIR *dir_stream = opendir(pathname);
        if(dir_stream) { 
            struct dirent *entry;
            while ((entry = readdir(dir_stream)) != NULL) {
                // on filtre . et ..
                if(strcmp(entry->d_name, ".") == 0 
                || strcmp(entry->d_name, "..") == 0) {
                    continue;
                }
                // parse path
                char path[PATH_MAX];
                snprintf(path, PATH_MAX, "%s/%s", pathname, entry->d_name);
                // read entry
                size += du_file(path);
            }
            closedir(dir_stream);
        }
    } else {
        size = du_single_file(pathname);
    }
    return size;    
}
