// #define _POSIX_C_SOURCE 2
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>

#include "answers.h"

int main(int argc, char *argv[])
{
    int opt;
    while((opt = getopt(argc, argv, "Lbh")) != -1) {
        switch (opt) {
            case 'L': opt_follow_links = 1; break;
            case 'b': opt_apparent_size = 1; break;
            case 'h': 
                printf(
                    "Usage: ./prg [...options] <file>\n"
                    "-h   Show this help prompt\n"
                    "-L   Follow sym links\n"
                    "-b   Show real size instead of block size\n"
                );
                exit(EXIT_SUCCESS);
            default:
                fprintf(stderr, "Unrecognised argument '%c'", opt);
                break;
        }
    }

    if (argc < optind) {
        fprintf(stderr, "The path is a required argument\n");
        exit(EXIT_FAILURE);
    }

    printf("The options are: opt_apparent_size = %d, opt_follow_links = %d\n",
           opt_apparent_size, opt_follow_links);
    
    int s = du_file(argv[optind]);
    printf("%d\t%s\n", s, argv[optind]);

    return EXIT_SUCCESS;
}
